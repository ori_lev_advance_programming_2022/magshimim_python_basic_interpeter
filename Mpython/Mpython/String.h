#pragma once
#include "Sequence.h"

#include <string>


class String : public Sequence
{
public:
	String(const std::string s);

	bool isPrintable() const override;
	std::string toString() const override;
	Type* copySelf() const override;
private:
	std::string _val;
};