#pragma once
#include "Type.h"


class Integer : public Type
{
public:
	Integer(int num);

	bool isPrintable() const override;
	std::string toString() const override;
	Type* copySelf() const override;
private:
	int _val;
};

