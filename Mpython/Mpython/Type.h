#pragma once

#include <string>


class Type
{
public:

	Type();
	virtual ~Type();

	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
	virtual Type* copySelf() const = 0;
	bool getTemp() const;
	void setTemp(bool t);
private:
	bool _temp;

};
