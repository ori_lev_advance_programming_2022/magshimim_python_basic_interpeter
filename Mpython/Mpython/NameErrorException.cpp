#include "NameErrorException.h"

NameErrorException::NameErrorException(const std::string& name) : InterpreterException()
{
    _name = name;
}

NameErrorException::~NameErrorException()
{
}

const char* NameErrorException::what() const noexcept
{
    std::string str = "NameError: name �" + _name + "� is not defined";
    return str.c_str();
}
