#pragma once
#include "Type.h"

class Sequence : public Type {
public:
	Sequence();

	bool isPrintable() const override;
	std::string toString() const override;
	Type* copySelf() const override;
};