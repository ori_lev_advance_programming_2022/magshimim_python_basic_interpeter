#include "Type.h"

Type::Type()
{
	_temp = false;
}

Type::~Type()
{
}

bool Type::getTemp() const
{
	return _temp;
}

void Type::setTemp(bool t)
{
	_temp = t;
}
