#include "String.h"

String::String(const std::string s) : Sequence()
{
	_val = s;
	_val[0] = '\'';
	_val[_val.size() - 1] = '\'';
}


bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	return _val;
}

Type* String::copySelf() const
{
	Type* t = new String(_val);
	return t;
}
