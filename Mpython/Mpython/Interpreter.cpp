#include "Type.h"
#include "InterpreterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "Parser.h"
#include <iostream>
#include <string>
#include <exception>


#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Ori Lev"


int main(int argc,char **argv)
{
	Type* t;
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// parsing command
		try {
			t = Parser::parseString(input_string);
			
			// check if it printable 
			if (t->isPrintable())
			{
				std::cout << t->toString() << std::endl;
			}
			// relaese temp objects
			if (t->getTemp())
			{
				delete t;
			}
		}
		catch (const std::exception &e) {
			std::cerr << e.what() << std::endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}
	// delete all the varibles
	Parser::deleteVaribles();
	return 0;
}
