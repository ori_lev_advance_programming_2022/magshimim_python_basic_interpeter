#include "Integer.h"

Integer::Integer(int num) : Type()
{
	_val = num;
}



bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	return std::to_string(_val);
}

Type* Integer::copySelf() const
{
	Type* t = new Integer(_val);
	return t;
}
