#include "Parser.h"
#include <iostream>
#include "IndentationException.h"
#include "String.h"
#include "Boolean.h"
#include "Integer.h"
#include "Helper.h"
#include "Void.h"
#include <iostream>
#include "SyntaxException.h"

#define CREATE_OBJECT_DELIMETER '='

std::unordered_map<std::string, Type*> Parser::_variables = std::unordered_map<std::string, Type*>();

/*
* Function parse the string and return his type
* input: string to parse
* output: a type object ptr
*/
Type* Parser::parseString(std::string str)
{
	Type* t;
	if (str[0] == ' ' || str[0] == '\t') {
		throw IndentationException();
	}

	// check if it only want to get the value of the varible
	t = getVariableValue(str);
	if (t != nullptr)
	{
		return t;
	}

	Helper::rtrim(str);
	t = getType(str);

	if (t == nullptr)
	{
		if (makeAssignment(str))
		{
			Void* v = new Void();
			v->setTemp(true);
			t = v;
		}
		else
			throw SyntaxException();
	}
	// in case that the input is just a varible
	else
		t->setTemp(true);
	return t;
}

/*
* function check what is the type of the object and create it
* string to parse
* object ptr
*/
Type* Parser::getType(std::string str)
{
	Type* ret = nullptr;


	if (Helper::isInteger(str)) {
		
		Helper::removeLeadingZeros(str);

		Integer* n = new Integer(stoi(str));
		ret = n;
	}

	else if (Helper::isBoolean(str))
	{
		bool isTrue = str == "True";
		Boolean* b = new Boolean(isTrue);

		ret = b;
	}

	else if (Helper::isString(str))
	{
		Helper::trim(str);
		String* s = new String(str);

		ret = s;
	}
	return ret;
}

/*
* function delete varibles
*/
void Parser::deleteVaribles()
{
	std::unordered_map<std::string, Type*>::iterator it;

	for (it = _variables.begin(); it != _variables.end(); it++)
		delete it->second;
}

/*
* function check if the varible name is legal
* input: name to check
* output: if its legal (bool)
*/
bool Parser::isLegalVarName(std::string str)
{
	if (str.length() == 0)
		return false;

	char temp = str[0];
	
	if (!(Helper::isLetter(temp) || Helper::isUnderscore(temp)))
		return false;

	for (std::size_t i = 1; i < str.length(); i++)
	{
		temp = str[i];

		if (!(Helper::isLetter(temp) || Helper::isDigit(temp) || Helper::isUnderscore(temp)))
			return false;
	}
	return true;
}


/*
* function check if a statment is to create varible if it is he insert him
* input: statment to check
* output: if the statment is ok
*/
bool Parser::makeAssignment(std::string str)
{
	std::string name = "";
	std::string value = "";
	Type* t;

	// split the string 
	std::size_t delimeter_index = str.find(CREATE_OBJECT_DELIMETER);

	if (delimeter_index == std::string::npos)
		return false;

	// get name and value
	name = str.substr(0, delimeter_index);
	value = str.substr(delimeter_index + 1, str.size() - 1);

	Helper::rtrim(name);
	Helper::trim(value);

	// no legal name
	if (!isLegalVarName(name))
	{
		throw SyntaxException();
		return false;
	}

	// if the line put varivle a in b (b = a)
	if (getVariableValue(value) != nullptr && getVariableValue(name) != nullptr)
	{
		// get their iters
		auto itName = _variables.find(name);
		auto itValue = _variables.find(value);
		// delete the old vaue
		std::string name = itName->first;
		delete itName->second;

		_variables.erase(itName);

		Type* temp = itValue->second->copySelf();
		// insert the new pair
		_variables.insert({ name, temp });
		return true;
	}

	// get the value as a Type*
	t = getType(value);
	if (t == nullptr)
		throw SyntaxException();

	t->setTemp(false);

	// replace the value / insert the pair
	auto it = _variables.find(name);
	if (it != _variables.end())
	{
		Type* t2 = it->second;
		it->second = t;
		delete t2;
	}
	else	
		_variables.insert({name, t});
	
	return true;
}

/*
* function return a ptr to varible if it exist
*/
Type* Parser::getVariableValue(std::string str)
{
	auto it = _variables.find(str);
	if (it != _variables.end())
		return it->second;
	return nullptr;
}
