#pragma once
#include "Type.h"


class Boolean : public Type 
{
public:
	Boolean(bool val);

	bool isPrintable() const override;
	std::string toString() const override;
	Type* copySelf() const override;
private:
	bool _value;
};