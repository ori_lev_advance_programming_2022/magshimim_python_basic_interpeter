#include "Boolean.h"

Boolean::Boolean(bool val) : Type()
{
	_value = val;
}


bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	return _value ? "True" : "False";
}

Type* Boolean::copySelf() const
{
	Type* t = new Boolean(_value);
	return t;
}
